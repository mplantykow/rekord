﻿using rekord.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rekord
{
    public enum Localizations { Domek, Piwnica, Szafa, Lodówka, Coścoś }
    public enum Points { Usunięty, Zniszczony, Normalny, Dobry, Idealny }
    public class Element
    {
        public string Name { get; set; }
        public string RekordNumber { get; set; }
        public Localizations Localization { get; set; }
        public string Comment { get; set; }
        public Points Point { get; set; }

        public Element()
        {
            Name = "";
            RekordNumber = "";
            Localization = Localizations.Domek;
            Comment = "";
            Point = Points.Usunięty;
        }

        public Element (string name, string rekordNumber, Localizations localization, string comment, Points point)
        {
            this.Name = name;
            this.RekordNumber = rekordNumber;
            this.Localization = localization;
            this.Comment = comment;
            this.Point = point;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

namespace rekord
{
    public partial class MainWindow : Window
    {
        public ObservableCollection <Element> ElementsList { get; set; }
        public User ActualUser { get; set; }
        int LogedInFlag = 0;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;               //databinding musi wiedziec co bajndowac 

            ElementsList = new ObservableCollection<Element>();

            this.LocalizationComboBox.ItemsSource = Enum.GetValues(typeof(Localizations)).Cast<Localizations>();
            this.LocalizationComboBox.SelectedIndex = 0;
            this.PointComboBox.ItemsSource = Enum.GetValues(typeof(Points)).Cast<Points>();
            this.PointComboBox.SelectedIndex = 0;

            ActualUser = new User("", "");

            IInterfejsNaSile obj = new InterfejsNaSile();
            obj.Metoda();

                LoggedAs.Text = "Zaloguj się";

        }

        public void LoginButon_Click(object sender, RoutedEventArgs e)
        {
            String UserName = this.LoginBox.Text;
            String UserPassword = this.PasswordBox.Password;

            string[,] Users = { { "User1", "user1" }, { "User2", "user2" }, { "User3", "user3" } };
            // ArrayList ListaUzytkownikow = new ArrayList();
            //foreach (string x in Users)
            // {
            //     ListaUzytkownikow.Add(x);  //Tu jest kolekcja 
            //  }
            if (LogedInFlag == 1)
            {
                MessageBox.Show("Uzytkownik jest zalogowany, wyloguj sie aby sie zalogowac na inne konto", "Error");
                User.WriteToDebugLog("uzytkownik zalogowany a probuje sie zalogowac");
            }
            else if (UserName == "")
            {
                MessageBox.Show("Wpisz login aby się zalogować", "Error");
                User.WriteToDebugLog("Użytkownik nie podał loginu");
            }
            else if (UserPassword == "")
            {
                MessageBox.Show("Brak hasła", "Error");
                User.WriteToDebugLog("Użytkownik nie podał hasła");
            }
            else
            {
                for (int i = 0; i < Users.GetLength(0) ; i++)
                {
                    if (UserName == Users[i, 0])
                    {
                        if (UserPassword == Users[i, 1])
                        {
                            MessageBox.Show("Użytkownik poprawnie zalogowany", "Success");
                            User.WriteToDebugLog("Użytkownik poprawnie zalogowany");
                            ActualUser.Login = UserName;
                            LogedInFlag = 1;
                            this.LoginBox.Text = "";
                            this.PasswordBox.Password = "";
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Błędne hasło", "Error");
                            User.WriteToDebugLog("Użytkownik podał błędne hasło");
                            break;
                        }
                    }
                    else
                    {
                        if (i == Users.Length)
                        {
                            MessageBox.Show("Błędny login", "Error");
                            User.WriteToDebugLog("Użytkownik podał błędny login");
                        }
                    }
                }

            }
            this.LoginBox.Text = "";
            this.PasswordBox.Password = "";

            if (LogedInFlag == 0)
            {
                LoggedAs.Text = "Zaloguj się";
            }
            else if (LogedInFlag == 1)
            {
                //LoggedAs.Text = "Zalogowany";
                LoggedAs.Text = ActualUser.Login;
            }
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (LogedInFlag ==0)
            {
                MessageBox.Show("Użytkownik nie jest zalogowany", "Message");
                User.WriteToDebugLog("Użytkownik nie jest zalogowany a probuje sie wylogowac");
            }
            if (LogedInFlag == 1)
            {
                LogedInFlag = 0;
                MessageBox.Show("Użytkownik poprawnie wylogowany", "Message");
                User.WriteToDebugLog("Użytkownik poprawnie wylogowany");
            }
            LoggedAs.Text = "Zaloguj się";
        }

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            String name = this.NameTextBox.Text;
            String rekordNumber = this.RekurdNumberTextBox.Text;
            Localizations localization = (Localizations)Enum.Parse(typeof(Localizations), this.LocalizationComboBox.Text);
            String comment = this.CommentTextbox.Text;
            Points point = (Points)Enum.Parse(typeof(Points), this.PointComboBox.Text);

            if (LogedInFlag == 1)
            {
                Element element = new Element(name, rekordNumber, localization, comment, point);
                ElementsList.Add(element);
                User.WriteToDebugLog("Element poprawnie dodany");
            }
            else
            {
                MessageBox.Show("Musisz być zalogowany, aby edytować liste", "Error");
                User.WriteToDebugLog("Element poprawnie usunięty");
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (LogedInFlag == 1)
            {
                try 
                    //jeśli lista jest pusta, ktos kliknie delete to sie wysypuje - naprawic
                    //jesli element nie jest wskazany, nie wyrzuca catch tylko sie wysypuje - naprawic
                {
                    this.ElementsList.RemoveAt(this.ListView1.SelectedIndex);
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Wybierz pozycje do usuniecia", "Error");
                    User.WriteToDebugLog("Nie wybrana pozycja do usuniecia");
                }
            }
            else
            {
                MessageBox.Show("Musisz być zalogowany, aby edytować liste", "Error");
                User.WriteToDebugLog("Próba dodania elementy przez nie zalogowanego użytkownika");
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if(LogedInFlag ==1 )
            {
                Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
                dialog.FileName = "ElementsList";
                dialog.DefaultExt = ".xml";
                dialog.Filter = "XML documents (.xml)|*.xml";

                Nullable<bool> result = dialog.ShowDialog();
                if (result == true)
                {
                    string filePath = dialog.FileName;
                    ListToXmlFile(filePath);
                    User.WriteToDebugLog("Lista poprawnie zapisana");
                }
            }

            else if (LogedInFlag == 0 )
            {
                MessageBox.Show("Musisz byc zalogowany aby zapisac zmiany");
                User.WriteToDebugLog("uzytkownik nie zalogowany probuje zapisac zmiany");
            }
        }

        private void ListToXmlFile (string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                var serializer = new XmlSerializer(typeof (ObservableCollection<Element>));
                serializer.Serialize(sw, ElementsList);
            }
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.FileName = "ElementsList";
            dialog.DefaultExt = ".xml";
            dialog.Filter = "XML documents (.xml)|*.xml";

            Nullable<bool> result = dialog.ShowDialog();
            string FileName = "";
            if (result == true)
            {
                FileName = dialog.FileName;
            }
            if (File.Exists(FileName))
            {
                XmlFileToList(FileName);
                User.WriteToDebugLog("Lista poprawnie załadowana");
            }
            else
            {
                MessageBox.Show("Wskazany plik nie istnieje", "Error");
            }
        }
        private void XmlFileToList(string FilePath)
        {
            using (var sr = new StreamReader(FilePath))
            {
                var deserializer = new XmlSerializer(typeof(ObservableCollection<Element>));
                ObservableCollection<Element> TemporaryList = (ObservableCollection<Element>) deserializer.Deserialize(sr);
                foreach (var item in TemporaryList)
                {
                    ElementsList.Add(item);
                }
            }
        }
    }
}

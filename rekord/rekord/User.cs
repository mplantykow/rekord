﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace rekord
{

    public class User 
    {
        public string Login { get; set; }
        public string Pasword { get; set; }

        public User()
        {
            Login = "";
            Pasword = "";
        }

        public User(string login, string password)
        {
            this.Login = login;
            this.Pasword = password;
        }

        //Method to save to debuglog only text message
        public static void WriteToDebugLog(string MessageToTextDebugLog)
        {
            if(File.Exists(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt"))
            {
                DateTime data = DateTime.Now;
                string s = File.ReadAllText(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt");
                File.WriteAllText(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt", s + " " + data + " " + MessageToTextDebugLog + "#");
            }
        }

        //Method to save to one debugLog text message, and to second debugLog status code in bytes
        public static void WriteToDebugLog(string MessageToTextDebugLog, byte[] MessageToStatusCodesDebugLog)
        {
            if (File.Exists(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt"))
            {
                DateTime data = DateTime.Now;
                string s = File.ReadAllText(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt");
                File.WriteAllText(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\TextDebugLog.txt", s + " " + data + " " + MessageToTextDebugLog + "#");
                //zsumowanie zawartosci tablic - naprawic
                //dodanie daty - naprawic
                //byte[] b =File.ReadAllBytes(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\DebugLog.txt");
                File.WriteAllBytes(@"C:\Users\Marta\Documents\Visual Studio 2015\Projects\rekord\BytesDebugLog.txt", MessageToStatusCodesDebugLog);
            }
        }
    }

    public class Admin : User
    {
        public Admin(string login, string password) : base ( login, password)
        {

        }
    }

}
